<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>'visitor'], function(){
	
	Route::get('/register','RegisterController@register');
	Route::post('/register','RegisterController@postRegister');
	Route::get('/login','LoginController@login');
	Route::post('/login','LoginController@postlogin');	
});



Route::post('/logout','LoginController@logout');


Route::group(['middleware'=>'admin'], function(){
	Route::get('/earnings','AdminController@earnings');
	
});


Route::group(['middleware'=>'manager'], function(){
	Route::get('/tasks','ManagerController@tasks');
	
});

Route::get('/activate/{email}/{activationCode}','ActivationController@activation');

